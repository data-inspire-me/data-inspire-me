class Routes {
  static const home = '/';
  static const quiz = '/quiz';
  static const about = '/about';
  static const fullScreenImage = '/fullScreenImage';
}
