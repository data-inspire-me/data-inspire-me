import 'package:data_inspire_me/models/data/question_types/question.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:latlng/latlng.dart';
import 'package:meta/meta.dart';
import 'package:quiver/core.dart';

part 'question_state.g.dart';

@immutable
@HiveType(typeId: 1)
class QuestionState extends Equatable {
  final Question? currentQuestion;
  final double currentScore;
  final int currentQuestionNum;
  final List<QuestionType> validCategories;
  final bool showingSolution;
  final LatLng? guessedLocation;
  final int? sc_selectedIndex;

  // TODO: Maybe change hardcoded validCategories to dynamic magic
  const QuestionState(
      {this.currentQuestion = null,
      this.currentScore = 0,
      this.currentQuestionNum = 0,
      this.showingSolution = false,
      this.guessedLocation = null,
      this.sc_selectedIndex = null,
      this.validCategories = const [
        QuestionType.geoguesser,
        QuestionType.guess_value,
        QuestionType.single_choice
      ]});

  QuestionState copyWith({
    Question? currentQuestion,
    List<QuestionType>? validCategories,
    double? currentScore,
    int? currentQuestionNum,
    Optional<int?>? sc_selectedIndex,
    Optional<LatLng?>? guessedLocation,
    bool? showingSolution,
  }) {
    return QuestionState(
      validCategories: validCategories ?? this.validCategories,
      currentScore: currentScore ?? this.currentScore,
      currentQuestionNum: currentQuestionNum ?? this.currentQuestionNum,
      currentQuestion: currentQuestion ?? this.currentQuestion,
      guessedLocation: guessedLocation != null
          ? guessedLocation.orNull
          : this.guessedLocation,
      sc_selectedIndex: sc_selectedIndex != null
          ? sc_selectedIndex.orNull
          : this.sc_selectedIndex,
      showingSolution: showingSolution ?? this.showingSolution,
    );
  }

  @override
  List<Object?> get props => [
        currentQuestion,
        currentScore,
        currentQuestionNum,
        validCategories,
        showingSolution,
        guessedLocation,
        sc_selectedIndex
      ];
}
