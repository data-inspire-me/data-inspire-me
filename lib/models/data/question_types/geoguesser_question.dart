import 'package:data_inspire_me/models/data/question_types/question.dart';
import 'package:latlng/latlng.dart';

class GeoguesserQuestion extends Question {
  final LatLng coords;

  GeoguesserQuestion(
      {required category,
      required openDataUrl,
      required questionText,
      required image,
      required this.coords,
      String? visualisueringLink})
      : super(
            type: QuestionType.geoguesser,
            category: category,
            openDataUrl: openDataUrl,
            questionText: questionText,
            image: image,
            visualisueringLink: visualisueringLink) {}

  @override
  List<Object?> get props => super.props
    ..addAll([
      coords,
    ]);
}
