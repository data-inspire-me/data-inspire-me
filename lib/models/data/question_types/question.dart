import 'package:equatable/equatable.dart';

enum QuestionType { geoguesser, guess_value, single_choice }

abstract class Question extends Equatable {
  final QuestionType type;
  final String category;
  final String openDataUrl;
  final String questionText;
  final String image;
  final String? imageSrc;
  final String? openDataDesc;
  final String? visualisueringLink;

  Question(
      {required this.type,
      required this.category,
      required this.openDataUrl,
      required this.questionText,
      required this.image,
      this.imageSrc,
      this.openDataDesc,
      this.visualisueringLink}) {}

  @override
  List<Object?> get props => [
        type,
        category,
        openDataUrl,
        questionText,
        image,
      ];
}
