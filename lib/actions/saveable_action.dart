class SaveableAction {
  final bool saveStateAfter;

  SaveableAction({required this.saveStateAfter});
}
