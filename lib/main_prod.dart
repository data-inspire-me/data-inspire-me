import 'package:data_inspire_me/main_lib.dart';
import 'package:data_inspire_me/models/app_environment.dart';

Future<void> main() async {
  await runWithEnvironment(AppEnvironment.PROD);
}
