import 'package:data_inspire_me/presentation/widgets/default_scaffold.dart';
import 'package:data_inspire_me/presentation/widgets/disable_scroll_animation.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return DefaultScaffold(
      body: Center(
        child: DisableScrollAnimation(
          child: ListView(
            children: [
              SizedBox(
                height: screenHeight * 0.08,
              ),
              Container(
                height: screenHeight * 0.2,
                child: Image.asset('assets/images/muensterhack-logo.png'),
              ),
              SizedBox(
                height: screenHeight * 0.03,
              ),
              Text(
                'home.app_title',
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ).tr(),
              SizedBox(
                height: screenHeight * 0.02,
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    'about.one_sentence_pitch',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.center,
                  ).tr(),
                ),
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    'about.info_text',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.center,
                  ).tr(),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blue),
                  text: 'Münsterhack',
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      launchUrl(Uri.parse('https://muensterhack.de/'));
                    },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blue),
                  text: 'GitLab',
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      launchUrl(Uri.parse(
                          'https://gitlab.com/data-inspire-me/data-inspire-me'));
                    },
                ),
              ),
              Container(
                constraints: BoxConstraints(
                  minHeight: 10,
                  maxHeight: 20,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
