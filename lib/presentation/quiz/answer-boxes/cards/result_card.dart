import 'package:flutter/material.dart';

import 'base_card.dart';

class ResultCard extends StatelessWidget {
  final bool? isCorrect;
  final String titleLabel;

  final Color green = const Color(0xff18d972);
  final Color red = const Color(0xffcc3d53);
  final Color grey = const Color(0xff484e71);

  const ResultCard({
    required this.titleLabel,
    required this.isCorrect,
  });

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      titleLabel: titleLabel,
      borderColor: isCorrect == null
          ? grey
          : isCorrect!
              ? green
              : red,
      iconColor: isCorrect == null
          ? grey
          : isCorrect!
              ? green
              : red,
      icon: isCorrect == true ? Icons.check : Icons.clear,
    );
  }
}
