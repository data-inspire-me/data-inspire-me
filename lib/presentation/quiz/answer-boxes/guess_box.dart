import 'package:data_inspire_me/models/data/question_types/guess_question.dart';
import 'package:data_inspire_me/models/data/question_types/question.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:tuple/tuple.dart';

class GuessBox extends StatefulWidget {
  @override
  GuessBoxState createState() => GuessBoxState();
}

class GuessBoxState extends State<GuessBox> {
  double curInput = 0;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Tuple2<Question?, bool>>(
      converter: (store) => Tuple2(
        store.state.questionState.currentQuestion,
        store.state.questionState.showingSolution,
      ),
      builder: (context, questionAndShowingSolution) {
        return Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 100, right: 100),
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: 300,
                ),
                child: TextFormField(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 40),
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  initialValue: curInput.toString(),
                  enabled: !questionAndShowingSolution.item2,
                  onChanged: (val) {
                    if (val == '') {
                      curInput = 0;
                      return;
                    }

                    curInput = double.parse(val);
                  },
                ),
              ),
            ),
            if (questionAndShowingSolution.item2)
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'guess.solution_label',
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ).tr(args: [
                  (questionAndShowingSolution.item1! as GuessQuestion)
                      .answer
                      .toString(),
                ]),
              ),
          ],
        );
      },
    );
  }
}
