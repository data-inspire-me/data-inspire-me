import 'dart:math';

import 'package:data_inspire_me/models/data/question_types/question.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:data_inspire_me/presentation/home/home_screen.dart';
import 'package:data_inspire_me/presentation/quiz/answer-boxes/answer_box.dart';
import 'package:data_inspire_me/presentation/widgets/default_scaffold.dart';
import 'package:data_inspire_me/presentation/widgets/full_screen_image/full_screen_capable_image.dart';
import 'package:data_inspire_me/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class QuizScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultScaffold(
      resizeToAvoidBottomInset: false,
      body: Builder(builder: (context) {
        final cardHeight = min<double>(
          MediaQuery.of(context).size.height,
          900,
        );
        return Center(
          child: StoreConnector<AppState, Question?>(
            converter: (store) => store.state.questionState.currentQuestion,
            builder: (context, currentQuestion) {
              // Return to home screen when page is reloaded
              if (currentQuestion == null) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (_) => HomeScreen(),
                    ),
                    (_) => false,
                  );
                });
                return SizedBox();
              }

              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: cardHeight,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: 20, left: 20, right: 20, bottom: 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(25)),
                            child: Container(
                              constraints: BoxConstraints(
                                maxHeight: cardHeight * 0.16,
                              ),
                              child: FullScreenCapableImage(
                                image: Image.network(
                                  currentQuestion!.image,
                                ).image,
                              ),
                            ),
                          ),
                        ),
                        Chip(
                          label: Text(currentQuestion.category),
                          backgroundColor: Theme.of(context).backgroundColor,
                        ),
                        Container(
                          height: cardHeight * 0.18,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Column(
                              children: [
                                Container(
                                  constraints: BoxConstraints(
                                    maxHeight: 10,
                                  ),
                                ),
                                Text(
                                  currentQuestion.questionText,
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                  textAlign: TextAlign.center,
                                  maxLines: 5,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: AnswerBox(),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        );
      }),
    );
  }
}
