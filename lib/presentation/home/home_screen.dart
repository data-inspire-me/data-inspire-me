import 'dart:math';

import 'package:data_inspire_me/actions/actions.dart';
import 'package:data_inspire_me/models/data/question_types/question.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:data_inspire_me/presentation/widgets/default_scaffold.dart';
import 'package:data_inspire_me/routes.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = min<double>(
      MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
      700,
    );

    return DefaultScaffold(
      body: Container(
        constraints: BoxConstraints(
          maxHeight: screenHeight,
        ),
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: screenHeight * 0.08,
              ),
              Container(
                height: screenHeight * 0.2,
                child: Image.asset('assets/images/muensterhack-logo.png'),
              ),
              SizedBox(
                height: screenHeight * 0.03,
              ),
              Text(
                'home.app_title',
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
              ).tr(),
              SizedBox(
                height: screenHeight * 0.02,
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    'home.welcome_text',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.center,
                  ).tr(),
                ),
              ),
              SizedBox(
                height: screenHeight * 0.07,
              ),
              StoreConnector<AppState,
                  void Function(List<QuestionType> validCategories)>(
                converter: (store) => (List<QuestionType> categories) =>
                    store.dispatch(StartQuizAction(categories)),
                builder: (context,
                    void Function(List<QuestionType> categories) startQuiz) {
                  return ElevatedButton(
                    onPressed: () {
                      startQuiz([
                        QuestionType.geoguesser,
                        QuestionType.guess_value,
                        QuestionType.single_choice
                      ]);
                      Navigator.of(context).pushNamed(Routes.quiz);
                    },
                    child: Text('home.all_categories_button').tr(),
                  );
                },
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextButton(
                        onPressed: () =>
                            Navigator.of(context).pushNamed(Routes.about),
                        child: Text(
                          'home.about_button',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ).tr(),
                      ),
                      TextButton(
                        onPressed: () => showLicensePage(context: context),
                        child: Text(
                          'home.license_button',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ).tr(),
                      ),
                      Container(
                        constraints: BoxConstraints(
                          maxHeight: 50,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
