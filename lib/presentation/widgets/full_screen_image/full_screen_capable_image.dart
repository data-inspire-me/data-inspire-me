import 'package:data_inspire_me/presentation/widgets/full_screen_image/full_screen_image_screen.dart';
import 'package:data_inspire_me/routes.dart';
import 'package:flutter/material.dart';

class FullScreenCapableImage extends StatelessWidget {
  final ImageProvider image;

  FullScreenCapableImage({
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(
          Routes.fullScreenImage,
          arguments: FullScreenImageScreenArgs(
            image,
          ),
        );
      },
      child: Image(image: image),
    );
  }
}
