import 'package:data_inspire_me/middleware/persistence/store_state_middleware.dart';
import 'package:data_inspire_me/middleware/question/question_middleware.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> middlewares = [
  ...storeMiddlewares,
  ...questionMiddlewares,
];
