import 'package:data_inspire_me/actions/saveable_action.dart';
import 'package:data_inspire_me/middleware/persistence/hive_storage.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> storeMiddlewares = [
  TypedMiddleware<AppState, SaveableAction>(_saveState),
];

void _saveState(
    Store<AppState> store, SaveableAction action, NextDispatcher next) {
  next(action);

  if (action.saveStateAfter) {
    HiveStorage().saveState(store.state);
  }
}
