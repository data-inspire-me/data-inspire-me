import 'package:data_inspire_me/models/app_environment.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:data_inspire_me/services/migrations/db_migration_service.dart';
import 'package:data_inspire_me/util/hive-util.dart';
import 'package:get_it/get_it.dart';

class HiveStorage {
  void saveState(AppState state) {
    HiveUtil().stateBox!.put('state', state);
  }

  AppState loadState(AppEnvironment env) {
    // TODO: @Felix Wäre cool, wenn Du es hinbekommst, dass die stateBox auf nicht nullable gesetzt werden kann, dann brauchen wir die null checks net
    var storedState = HiveUtil().stateBox!.get('state');

    if (storedState == null) {
      return AppState(appEnvironment: env);
    }

    storedState = GetIt.I.get<DBMigrationService>().migrateState(storedState);

    return storedState.copyWith(appEnvironment: env);
  }
}
