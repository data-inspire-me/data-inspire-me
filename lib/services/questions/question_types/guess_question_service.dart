import 'dart:math';

import 'package:data_inspire_me/models/data/question_types/guess_question.dart';
import 'package:data_inspire_me/services/questions/question_types/question_service_template.dart';

class GuessQuestionService implements QuestionServiceTemplate {
  GuessQuestion getRandomQuestion() {
    return getQuestions()[Random().nextInt(getQuestions().length)];
  }

  List<GuessQuestion> getQuestions() {
    return [
      GuessQuestion(
        category: 'Abfall in Münster',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/abfallbilanz-2020-der-abfallwirtschaftsbetriebe-m%C3%BCnster',
        questionText:
            'Wie hoch ist das Abfallaufkommen pro Einwohner im Jahr 2020 (in kg)?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/a/ae/Material_recovery_facility_2004-03-24.jpg',
        imageSrc:
            'https://commons.wikimedia.org/wiki/File:Material_recovery_facility_2004-03-24.jpg, gemeinfrei',
        answer: 422,
      ),
      GuessQuestion(
        category: 'Abfall in Münster',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/abfallbilanz-2020-der-abfallwirtschaftsbetriebe-m%C3%BCnster',
        questionText:
            'Wie hoch war das Biomüllaufkommen in Privathaushalten in Münster im Jahr 2020 (in t)?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/a/ae/Material_recovery_facility_2004-03-24.jpg',
        imageSrc:
            'https://commons.wikimedia.org/wiki/File:Material_recovery_facility_2004-03-24.jpg, gemeinfrei',
        answer: 15495,
      ),
      GuessQuestion(
        category: 'Abfall in Münster',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/abfallbilanz-2020-der-abfallwirtschaftsbetriebe-m%C3%BCnster',
        questionText:
            'Wie viele Tonnen Altkleider wurden durch Privathaushalte entsorgt?',
        answer: 1348,
        image:
            'https://upload.wikimedia.org/wikipedia/commons/a/ae/Material_recovery_facility_2004-03-24.jpg',
        imageSrc:
            'https://commons.wikimedia.org/wiki/File:Material_recovery_facility_2004-03-24.jpg, gemeinfrei',
      ),
      // Fahrräder in Münster
      GuessQuestion(
        category: 'Fahrräder',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/abfallbilanz-2020-der-abfallwirtschaftsbetriebe-m%C3%BCnster',
        questionText:
            'Wie viele Fahrräder fuhren am 01.01.2021 zwischen 0:00 und 1:00 Uhr über die Hammerstraße?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/e/e4/Muenster_Fahrradschleuse_4292.jpg',
        imageSrc:
            'CC-BY-SA https://commons.wikimedia.org/wiki/File:Muenster_Fahrradschleuse_4292.jpg, von Rüdiger Wölk',
        answer: 37,
      ),
      GuessQuestion(
        category: 'Fahrräder',
        openDataUrl: 'https://github.com/od-ms/radverkehr-zaehlstellen',
        questionText:
            'Wie viele Fahrräder fuhren am 01.01.2021 zwischen 0:00 und 1:00 Uhr über die Warendorfer Straße?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/e/e4/Muenster_Fahrradschleuse_4292.jpg',
        imageSrc:
            'CC-BY-SA https://commons.wikimedia.org/wiki/File:Muenster_Fahrradschleuse_4292.jpg, von Rüdiger Wölk',
        answer: 46,
      ),
      GuessQuestion(
        category: 'Abfall in Münster',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/abfallbilanz-2020-der-abfallwirtschaftsbetriebe-m%C3%BCnster',
        questionText:
            'Wie viele Fahrräder fuhren in der Nacht vom 24.12 auf den 25.12 (20:00 bis 4:00) über die Promenade?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/c/ce/MuensterPromenade26.jpg?uselang=de',
        imageSrc:
            'CC-BY-SA https://commons.wikimedia.org/wiki/File:MuensterPromenade26.jpg?uselang=de, von Rüdiger Wölk',
        answer: 13,
      ),
      // Münsterstraßen
      GuessQuestion(
        category: 'Müsteraner Straßen',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/stra%C3%9Fenliste/resource/722c24df-3477-4c6e-801d-d1dc55e36d64',
        questionText:
            'Wie viele Straßennamen beginnen in Münster mit “An der/ dem”',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/e/e4/Muenster_Fahrradschleuse_4292.jpg',
        imageSrc:
            'CC-BY-SA https://commons.wikimedia.org/wiki/File:Muenster_Fahrradschleuse_4292.jpg, von Rüdiger Wölk',
        answer: 25,
      ),
      GuessQuestion(
        category: 'Diverses',
        openDataUrl: '', // TODO: Add open data url for guess questions
        questionText:
            'Wie stark nahm die CO2-Emission in Münster 2019 im vergleich zum Vorjahr ab (in %)?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/9/9c/Muenster_Innenstadt.jpg',
        imageSrc:
            'CC-BY-SA-3.0 https://commons.wikimedia.org/wiki/File:Muenster_Innenstadt.jpg, Bernhard Kils',
        answer: 27.8,
      ),
      GuessQuestion(
        category: 'Diverses',
        openDataUrl: '', // TODO: Add open data url for guess questions
        questionText:
            'Wie hoch ist der Anteil der Personen die zwischen 5 und 19 Jahre lang im Bahnhofsviertel wohnen (in %)?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/9/9c/Muenster_Innenstadt.jpg',
        imageSrc:
            'CC-BY-SA-3.0 https://commons.wikimedia.org/wiki/File:Muenster_Innenstadt.jpg, Bernhard Kils',
        answer: 19.79,
      ),
      GuessQuestion(
        category: 'Diverses',
        openDataUrl: '', // TODO: Add open data url for guess questions
        questionText:
            'Wie hoch war die durchschnittliche Wartezeit in Minuten im Bürgerbüro am 1.10.2019?',
        image: '',
        answer: 21,
      ),
      GuessQuestion(
        category: 'Diverses',
        openDataUrl: '', // TODO: Add open data url for guess questions
        questionText: 'Wie groß ist der Spielplatz am Gallenkamp? (in m^2)',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/M%C3%BCnster%2C_Historisches_Rathaus_--_2014_--_6855.jpg/1024px-M%C3%BCnster%2C_Historisches_Rathaus_--_2014_--_6855.jpg',
        imageSrc:
            ' Dietmar Rabich (https://commons.wikimedia.org/wiki/File:Münster,_Historisches_Rathaus_--_2014_--_6855.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode',
        answer: 727,
      ),
      GuessQuestion(
        category: 'Diverses',
        openDataUrl: '', // TODO: Add open data url for guess questions
        questionText: 'Wie viele Neumeldungen gab es im Bereich Hansaplatz 202',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/6/6f/MuensterHansaviertelSoesterStrasse.jpg',
        imageSrc:
            'STBR (https://commons.wikimedia.org/wiki/File:MuensterHansaviertelSoesterStrasse.jpg), „MuensterHansaviertelSoesterStrasse“, https://creativecommons.org/licenses/by-sa/3.0/legalcode',
        answer: 57,
      ),
      // Wohnungsfertigstellungen und Abbrüche
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele Wohnungsabbrüche gab es in Mauritz-West im Jahr 2016?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/7/75/Muenster%2CFernmeldeturm9420.JPG',
        imageSrc:
            'Rüdiger Wölk (https://commons.wikimedia.org/wiki/File:Muenster,Fernmeldeturm9420.JPG), „Muenster,Fernmeldeturm9420“, https://creativecommons.org/licenses/by-sa/2.0/de/legalcode',
        answer: 5,
      ),
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele Wohnungsabbrüche gab es im Geistviertel im Jahr 2016?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/5/52/Muenster_Braun-Hogenberg.jpg',
        imageSrc:
            ' Georg Braun & Franz Hogenberg (https://commons.wikimedia.org/wiki/File:Muenster_Braun-Hogenberg.jpg), „Muenster Braun-Hogenberg“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
        answer: 19,
      ),
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele Wohnungen wurden in Angelmodde im Jahr 2019 fertiggestellt?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/5/52/Muenster_Braun-Hogenberg.jpg',
        imageSrc:
            ' Georg Braun & Franz Hogenberg (https://commons.wikimedia.org/wiki/File:Muenster_Braun-Hogenberg.jpg), „Muenster Braun-Hogenberg“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
        answer: 30,
      ),
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele 1-Raum-Wohnungen wurden im Jahr 2019 im Bahnhofsviertel fertiggestellt?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/5/52/Muenster_Braun-Hogenberg.jpg',
        imageSrc:
            ' Georg Braun & Franz Hogenberg (https://commons.wikimedia.org/wiki/File:Muenster_Braun-Hogenberg.jpg), „Muenster Braun-Hogenberg“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
        answer: 104,
      ),
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele 1-Raum-Wohnungen wurden im Jahr 2019 im Bahnhofsviertel fertiggestellt?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/5/52/Muenster_Braun-Hogenberg.jpg',
        imageSrc:
            ' Georg Braun & Franz Hogenberg (https://commons.wikimedia.org/wiki/File:Muenster_Braun-Hogenberg.jpg), „Muenster Braun-Hogenberg“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
        answer: 104,
      ),
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele Wohnungen wurden im Jahr 2018 in Mecklenbeck fertiggestellt?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/5/52/Muenster_Braun-Hogenberg.jpg',
        imageSrc:
            ' Georg Braun & Franz Hogenberg (https://commons.wikimedia.org/wiki/File:Muenster_Braun-Hogenberg.jpg), „Muenster Braun-Hogenberg“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
        answer: 136,
      ),
      GuessQuestion(
        category: 'Wohnungsfertigstellungen und Abbrüche',
        openDataUrl:
            'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
        questionText:
            'Wie viele Wohnungen wurden im Jahr 2019 in Coerde fertiggestellt?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/5/52/Muenster_Braun-Hogenberg.jpg',
        imageSrc:
            ' Georg Braun & Franz Hogenberg (https://commons.wikimedia.org/wiki/File:Muenster_Braun-Hogenberg.jpg), „Muenster Braun-Hogenberg“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
        answer: 8,
      ),
    ];
  }
}
