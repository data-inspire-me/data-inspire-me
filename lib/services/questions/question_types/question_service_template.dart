import 'package:data_inspire_me/models/data/question_types/question.dart';

abstract class QuestionServiceTemplate {
  List<Question> getQuestions();
  Question getRandomQuestion();
}
