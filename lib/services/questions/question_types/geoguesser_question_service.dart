import 'dart:math';

import 'package:data_inspire_me/models/data/question_types/geoguesser_question.dart';
import 'package:data_inspire_me/services/questions/question_types/question_service_template.dart';
import 'package:latlng/latlng.dart';

class GeoguesserQuestionService implements QuestionServiceTemplate {
  List<GeoguesserQuestion> getQuestions() {
    return [
      GeoguesserQuestion(
        category: 'Finde den Ort',
        openDataUrl:
            'https://upload.wikimedia.org/wikipedia/commons/8/80/M%C3%BCnster%2C_Erbdrostenhof_--_2020_--_6745.jpg',
        questionText: 'Wo befindet sich dieses bekannte Gebäude?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/8/80/M%C3%BCnster%2C_Erbdrostenhof_--_2020_--_6745.jpg',
        coords: LatLng(51.961238, 7.632125),
      ),
      GeoguesserQuestion(
        category: 'Finde den Ort',
        openDataUrl:
            'https://commons.wikimedia.org/wiki/Category:Quality_images_of_buildings_in_M%C3%BCnster_(Westfalen)#/media/File:M%C3%BCnster,_Freilichtmuseum_M%C3%BChlenhof,_Bockwindm%C3%BChle_--_2018_--_2153.jpg',
        questionText: 'Wo befindet sich diese Windmühle?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/M%C3%BCnster%2C_Freilichtmuseum_M%C3%BChlenhof%2C_Bockwindm%C3%BChle_--_2018_--_2153.jpg/1920px-M%C3%BCnster%2C_Freilichtmuseum_M%C3%BChlenhof%2C_Bockwindm%C3%BChle_--_2018_--_2153.jpg',
        coords: LatLng(51.949702, 7.598975),
      ),
      GeoguesserQuestion(
        category: 'Finde den Ort',
        openDataUrl:
            'https://commons.wikimedia.org/wiki/Category:Quality_images_of_objects_in_M%C3%BCnster_(Westfalen)#/media/File:M%C3%BCnster,_Aasee_--_2018_--_1788.jpg',
        questionText: 'Wo befindet sich dieses Objekt?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/M%C3%BCnster%2C_Aasee_--_2018_--_1788.jpg/1280px-M%C3%BCnster%2C_Aasee_--_2018_--_1788.jpg',
        coords: LatLng(51.953845, 7.61129),
      ),
      GeoguesserQuestion(
        category: 'Finde den Ort',
        openDataUrl:
            'https://commons.wikimedia.org/wiki/Category:Quality_images_of_objects_in_M%C3%BCnster_(Westfalen)#/media/File:M%C3%BCnster,_Alte_Feuerwache_--_2015_--_5883.jpg',
        questionText: 'Wo befindet sich dieses Schild?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/M%C3%BCnster%2C_Alte_Feuerwache_--_2015_--_5883.jpg/1280px-M%C3%BCnster%2C_Alte_Feuerwache_--_2015_--_5883.jpg',
        coords: LatLng(51.951672, 7.637568),
      ),
      GeoguesserQuestion(
        category: 'Finde den Ort',
        openDataUrl:
            'https://commons.wikimedia.org/wiki/Category:Quality_images_of_buildings_in_M%C3%BCnster_(Westfalen)#/media/File:M%C3%BCnster,_Hafen_--_2017_--_1814.jpg',
        questionText: 'Wo befindet sich dieser Ort?',
        image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/M%C3%BCnster%2C_Hafen_--_2017_--_1814.jpg/1280px-M%C3%BCnster%2C_Hafen_--_2017_--_1814.jpg',
        coords: LatLng(51.950725, 7.640608),
      ),
    ];
  }

  GeoguesserQuestion getRandomQuestion() {
    return getQuestions()[Random().nextInt(getQuestions().length)];
  }
}
