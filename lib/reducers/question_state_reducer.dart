import 'package:data_inspire_me/actions/actions.dart';
import 'package:data_inspire_me/models/states/question_state.dart';
import 'package:quiver/core.dart';
import 'package:redux/redux.dart';

final questionStateReducer = combineReducers<QuestionState>([
  TypedReducer<QuestionState, SetCurrentQuestionAction>(
    _onSetCurrentQuestion,
  ),
  TypedReducer<QuestionState, StartQuizAction>(
    _onStartQuiz,
  ),
  TypedReducer<QuestionState, SetCurrentQuestionNumAction>(
    _onSetCurrentQuestionNum,
  ),
  TypedReducer<QuestionState, SetCurrentScoreAction>(
    _onSetCurrentScore,
  ),
  TypedReducer<QuestionState, SwitchToNextQuestionAction>(
    _onSwitchToNextQuestion,
  ),
  TypedReducer<QuestionState, SetShowingSolutionAction>(
    _onSetShowingSolution,
  ),
  TypedReducer<QuestionState, SetGuessedLocationAction>(
    _onSetGuessedLocation,
  ),
  TypedReducer<QuestionState, SetSCSelectedAnswerIdxAction>(
    _onSetSelectedAnswerIdx,
  )
]);

QuestionState _onStartQuiz(
    QuestionState questionState, StartQuizAction action) {
  return questionState.copyWith(
    validCategories: action.validCategories,
    currentScore: 0,
    showingSolution: false,
  );
}

QuestionState _onSetShowingSolution(
    QuestionState questionState, SetShowingSolutionAction action) {
  return questionState.copyWith(showingSolution: action.showingSolution);
}

QuestionState _onSwitchToNextQuestion(
    QuestionState questionState, SwitchToNextQuestionAction action) {
  return questionState.copyWith(
    currentQuestionNum: action.questionNum,
    showingSolution: false,
    guessedLocation: Optional.absent(),
    sc_selectedIndex: Optional.absent(),
  );
}

QuestionState _onSetCurrentQuestion(
    QuestionState questionState, SetCurrentQuestionAction action) {
  return questionState.copyWith(currentQuestion: action.currentQuestion);
}

QuestionState _onSetCurrentQuestionNum(
    QuestionState questionState, SetCurrentQuestionNumAction action) {
  return questionState.copyWith(currentQuestionNum: action.questionNum);
}

QuestionState _onSetCurrentScore(
    QuestionState questionState, SetCurrentScoreAction action) {
  return questionState.copyWith(currentScore: action.score);
}

QuestionState _onSetGuessedLocation(
    QuestionState questionState, SetGuessedLocationAction action) {
  return questionState.copyWith(
      guessedLocation: Optional.fromNullable(action.guessedLocation));
}

QuestionState _onSetSelectedAnswerIdx(
    QuestionState questionState, SetSCSelectedAnswerIdxAction action) {
  return questionState.copyWith(
    sc_selectedIndex: Optional.fromNullable(action.selectedIdx),
  );
}
