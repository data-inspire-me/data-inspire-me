import 'package:data_inspire_me/actions/actions.dart';
import 'package:data_inspire_me/models/states/app_state.dart';
import 'package:data_inspire_me/reducers/question_state_reducer.dart';
import 'package:redux/redux.dart';

AppState appReducer(AppState state, action) {
  return state.copyWith(
    counter: counterReducer(state.counter, action),
    questionState: questionStateReducer(state.questionState, action),
  );
}

final counterReducer = combineReducers<int>([
  TypedReducer<int, SetCounterAction>(
    _onSetCounter,
  )
]);

int _onSetCounter(int counter, SetCounterAction action) {
  return action.counter;
}
